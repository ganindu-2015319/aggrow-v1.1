<?php
include_once '../classes/climateForecast.php';
include_once '../classes/crop.php';
include_once '../classes/user.php';

//Sample forecast data
$temp=[25,25,25,25,27,27,27,27,29,29,29,29,29,29,29,29,29,29,29,29,29,28,28,28,28,27,27,27,27,27,28,28,28,28,28,28,28,28,28,27,27,27,27,27,27,27,27,25,25,25,25,25,25,25,25,25,27,27,27,27,29,29,29,29,29,29,29,29,29,29,29,29,29,28,28,28,28,27,27,27,27,27,28,28,28,28,28,28,28,28,28,27,27,27,27,27,27,27,27,25,25,25,25,25];
$rain=[8.5,8.5,8.5,8.5,4.0,4.0,4.0,4.0,3.5,3.5,3.5,3.5,6.0,7.0,7.0,7.0,7.0,3.4,3.4,3.4,3.4,3.4,3.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0,5.0,6.0,6.0,6.0,6.0,13.0,13.0,13.0,13.0,24.0,24.0,24.0,24.0,24.0,16.0,16.0,16.0,16.0,8.5,8.5,8.5,8.5,4.0,4.0,4.0,4.0,3.5,3.5,3.5,3.5,7.4,7.6,7.8,8.0,8.2,3.4,3.4,3.4,3.4,6.2,6.4,6.6,6.9,7.1,7.3,7.5,7.7,7.9,8.1,8.3,8.5,8.7,9.0,9.2,9.4,9.6,9.8,13.0,19.5,19.7,19.8,19.9,20.1,20.2,20.4,20.5,20.7,20.8,20.9,21.1];
//var_dump(count($rain));
$climate=new climateForecast();
$climate->setYear(2020);
$climate->setTempForecast($temp);
$climate->setRainForecast($rain);


//Sample Crops

$riceTempTolerableMin=[20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20];
$riceTempTolerableMax=[40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40,40];
$riceTempOptimumMin=[23,23,23,23,24,24,24,24,24,25,25,25,26,27,27,27,27,27];
$riceTempOptimumMax=[30,30,30,30,30,30,31,31,31,32,32,32,32,32,33,33,33,33];

$riceRainTolerableMin=[2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,0,0];
$riceRainTolerableMax=[12,12,12,12,12,12,12,12,16,16,16,16,16,11,11,11,11,11];
$riceRainOptimumMin=[3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4];
$riceRainOptimumMax=[10,10,10,10,10,10,10,10,15,15,15,15,15,10,10,10,10,10];

//Sample crop1
$crop1=new crop();
$crop1->setName("rice");
$crop1->setRainTolerableMax($riceRainTolerableMax);
$crop1->setRainTolerableMin($riceRainTolerableMin);
$crop1->setRainOptimumMax($riceRainOptimumMax);
$crop1->setRainOptimumMin($riceRainOptimumMin);
$crop1->setTempOptimumMax($riceTempOptimumMax);
$crop1->setTempOptimumMin($riceTempOptimumMin);
$crop1->setTempTolerableMax($riceTempTolerableMax);
$crop1->setTempTolerableMin($riceTempTolerableMin);
$crop1->addCompatibleCrop("wheat");
$crop1->setWeeksToHarvest(18);

//Sample crop2
$crop2=new crop();
$crop2->setName("wheat");
$crop2->setRainTolerableMax($riceRainTolerableMax);
$crop2->setRainTolerableMin($riceRainTolerableMin);
$crop2->setRainOptimumMax($riceRainOptimumMax);
$crop2->setRainOptimumMin($riceRainOptimumMin);
$crop2->setTempOptimumMax($riceTempOptimumMax);
$crop2->setTempOptimumMin($riceTempOptimumMin);
$crop2->setTempTolerableMax($riceTempTolerableMax);
$crop2->setTempTolerableMin($riceTempTolerableMin);
$crop2->addCompatibleCrop("rice");
$crop2->setWeeksToHarvest(18);

//Sample Crop3
$crop3=new crop();
//$crop3=$crop2;
$crop3->setRainTolerableMax($riceRainTolerableMax);
$crop3->setRainTolerableMin($riceRainTolerableMin);
$crop3->setRainOptimumMax($riceRainOptimumMax);
$crop3->setRainOptimumMin($riceRainOptimumMin);
$crop3->setTempOptimumMax($riceTempOptimumMax);
$crop3->setTempOptimumMin($riceTempOptimumMin);
$crop3->setTempTolerableMax($riceTempTolerableMax);
$crop3->setTempTolerableMin($riceTempTolerableMin);
$crop3->addCompatibleCrop("rice");
$crop3->setWeeksToHarvest(18);
$crop3->setName("black gram");


//Sample Crop4
$crop4=new crop();
//$crop3=$crop2;
$crop4->setRainTolerableMax([-5]);
$crop4->setRainTolerableMin([-10]);
$crop4->setRainOptimumMax([-6]);
$crop4->setRainOptimumMin([-7]);
$crop4->setTempOptimumMax([100]);
$crop4->setTempOptimumMin([90]);
$crop4->setTempTolerableMax([120]);
$crop4->setTempTolerableMin([85]);
$crop4->setWeeksToHarvest(1);
$crop4->setName("maize");


$farmer=new user();
$farmer->setUserName("Test_Farmer");

$listOfCrops=array($crop1, $crop2, $crop3);

session_start();

?>