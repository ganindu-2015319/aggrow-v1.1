<?php
include_once 'header.php';
//var_dump($_SESSION['primaryCrop']);
?>
<div class="w3-container">
    <h2>Secondary Crop Recommendations</h2>
    <?php
    $farmer=$_SESSION['user'];
    $primaryCrop=$_SESSION['primaryCrop'];
    $secondaries= $farmer->getSecondaryCrops();
    ?>
    <form action="#" method="post">
        <select class="w3-select" name="secondaryCrop[]">
    <?php
    foreach ($secondaries as $entry){
        $cropName=$entry['crop']->getName();
        echo '<option value="' . $cropName . '">';
        echo $cropName. '</option>';
    }
    ?>
        </select>
        <br>
        <input type="submit" name="confirmSecondaryCrop" value="Confirm Secondary Crop" class="w3-button btn-primary"/>
    </form>

    <?php
    if(isset($_POST['confirmSecondaryCrop'])) {
        foreach ($_POST['secondaryCrop'] as $select) {
            $farmer->setSecondaryCrop($select);
            $_SESSION['plantSecondary']=$entry['week'];
            $_SESSION['secondaryCrop']=$farmer->getSecondaryCrop();
            header('Location: selections.php');
        }
    }
    ?>

</div>
</body>
</html>
