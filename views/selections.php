<?php
include 'header.php';
$primaryName=ucwords($_SESSION['primaryCrop']->getName());
$primaryWeeksToHarvest=$_SESSION['primaryCrop']->getWeeksToHarvest();
$plantPrimary=$_SESSION['plantPrimary']+1;
$secondaryName=ucwords($_SESSION['secondaryCrop']->getName());
$secondaryWeeksToHarvest=$_SESSION['secondaryCrop']->getWeeksToHarvest();
$plantSecondary=$_SESSION['plantSecondary']+1;
?>
<div class="w3-container">
    <h2>Primary Crop</h2>
    <table class="w3-table">
        <tr class=>
            <td>
                <h5><?php echo $primaryName; ?></h5>
            </td>
        </tr>
        <tr>
            <td>
                <p>Ideal to plant during week: <?php echo $plantPrimary; ?> of this year</p>
                <br>
                <p class="tinyTxt">Weeks till harvest: <?php echo $primaryWeeksToHarvest?> </p>
            </td>
        </tr>
    </table>
    <hr>
    <h2>Secondary Crop</h2>
    <table class="w3-table">
        <tr>
            <td>
                <h5><?php echo $secondaryName; ?></h5>
            </td>
        </tr>
        <tr>
            <td>
                <p>Ideal to plant during week: <?php echo $plantSecondary; ?> of this year</p>
                <br>
                <p class="tinyTxt">Weeks till harvest: <?php echo $secondaryWeeksToHarvest?> </p>
            </td>
        </tr>
    </table>
</div>
</body>
</html>