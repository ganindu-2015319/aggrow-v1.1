<?php
include_once '../scripts/init.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>AgGrow</title>
    <link rel="stylesheet" type="text/css" href="../style/main.css">
    <link rel="stylesheet" href=../style/w3.css>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="header">
    <img src="../images/logo-wide.png" class="header-img">
</div>