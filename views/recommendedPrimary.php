<?php
include "header.php";
?>
<div class="w3-container" >
    <h2>Primary Crops Recommendations</h2>
    <?php
    $primaries=$farmer->getPrimaryCrops($climate, $listOfCrops);
    ?>

    <form action="#" method="post">
        <select class="w3-select" name="primaryCrop[]">
    <?php
    foreach ($primaries as $entry){
        $cropName=$entry['crop']->getName();
        echo '<option value="' . $cropName . '">';
        echo $cropName. '</option>';
    }
    ?>
        </select>
        <br>
        <input type="submit" name="confirmPrimaryCrop" value="Confirm Primary Crop" class="w3-button btn-primary"/>
    </form>

    <?php
    if(isset($_POST['confirmPrimaryCrop'])) {
        foreach ($_POST['primaryCrop'] as $select) {
            $farmer->setPrimaryCrop($select);
            $_SESSION['plantPrimary']=$entry['week'];
            $_SESSION['user']=$farmer;
            $_SESSION['primaryCrop']=$farmer->getPrimaryCrop();
            header('Location: recommendedSecondary.php');
        }
    }

    ?>

<?php
//    $farmer->setPrimaryCrop($primaries[0]['crop']);
//    echo "<br> Secondary </br>";
//    $secondary=$farmer->getSecondaryCrops();
//    ?>


</div>
</body>

</html>
