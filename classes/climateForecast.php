<?php
/**
 * Created by PhpStorm.
 * User: Stig
 * Date: 27/04/2019
 * Time: 12:59 AM
 */

class climateForecast
{
    var $year;
    var $tempForecast= array();
    var $rainForecast= array();


//determines compatibility based on 2 parameters. Rain and Temperature.
//If either is out of tolerable range, compatibility['level']=0
//If both are within tolerable range, compatibility['level']=1
//If both are within optimal range, compatibility['level']=2
//returns associative array containing 'level' of compatibility (0,1,3)
//'found'=1 if found in either of 2 levels
//'week' with array index of week when crop should be planted

    public function cropCompatibility(crop $crop){
        $compatibility=array();
        if ($this->isOptimal($crop)['found']==1){
            $compatibility=$this->isOptimal($crop);
            $compatibility['level']=2;
        }
        elseif ($this->isTolerable($crop)==true){
            $compatibility=$this->isTolerable($crop);
            $compatibility['level']=1;
        }
        else
            $compatibility['level']=0;
        return $compatibility;
    }

    public function isOptimal(crop $crop){
        $matched=0;
        for($keyAv=0; $keyAv < count($this->tempForecast); $keyAv++){
            for($keyReq=0; $keyReq < $crop->getWeeksToHarvest(); $keyReq++){
                $tempRange=$crop->tempOptimum($keyReq);
                $rainRange=$crop->rainOptimum($keyReq);
                $tempAv=$this->tempForecast[$keyAv+$keyReq];
                $rainAv=$this->rainForecast[$keyAv+$keyReq];
                if($tempAv<=$tempRange["max"] && $tempAv>=$tempRange["min"] && $rainAv<=$rainRange["max"] && $rainAv>=$rainRange["min"]){
                    $matched=1;
                    continue;
                }
                else{
                    $matched=0;
                    break;
                }
            }
            if ($matched==1){
                $retVal['found']=1;
                $retVal['week']=$keyAv;
                return $retVal;
                break;
            }
        }
        if ($matched==0)
            return 0;
    }
    public function isTolerable(crop $crop){
        $matched=0;
        for($keyAv=0; $keyAv < count($this->tempForecast); $keyAv++){
            for($keyReq=0; $keyReq < $crop->getWeeksToHarvest(); $keyReq++){
                $tempRange=$crop->tempTolerable($keyReq);
                $rainRange=$crop->rainTolerable($keyReq);
                $tempAv=$this->tempForecast[$keyAv+$keyReq];
                $rainAv=$this->rainForecast[$keyAv+$keyReq];
                if($tempAv<=$tempRange["max"] && $tempAv>=$tempRange["min"] && $rainAv<=$rainRange["max"] && $rainAv>=["min"]){
                    $matched=1;
                    continue;
                }
                else{
                    $matched=0;
                    break;
                }
            }
            if ($matched==1){
                $retVal['found']=1;
                $retVal['week']=$keyAv;
                return $retVal;
                break;
            }
        }
        if ($matched==0)
            return 0;
    }

    public function setTempForecast($tempForecast)
    {
        $this->tempForecast = $tempForecast;
    }
    public function getTempForecast()
    {
        return $this->tempForecast;
    }

    public function setRainForecast($rainForecast)
    {
        $this->rainForecast = $rainForecast;
    }
    public function getRainForecast()
    {
        return $this->rainForecast;
    }
    public function setYear($year)
    {
        $this->year = $year;
    }
    public function getYear()
    {
        return $this->year;
    }
}