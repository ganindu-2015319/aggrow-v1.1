<?php
/**
 * Created by PhpStorm.
 * User: Stig
 * Date: 26/04/2019
 * Time: 08:41 PM
 */

class crop
{
    var $name;
    var $weeksToHarvest;
    public $tempOptimumMax = array();
    public $tempOptimumMin = array();
    public $tempTolerableMax = array();
    public $tempTolerableMin = array();
    public $rainOptimumMax = array();
    public $rainOptimumMin = array();
    public $rainTolerableMax = array();
    public $rainTolerableMin = array();
    public $compatibleCrops = array();

    public function addCompatibleCrop($cropName){
        $this->compatibleCrops[]=$cropName;
    }
    public function getCompatibleCrops(){
        return $this->compatibleCrops;
    }
    //Returns optimal temperature range for a particular week
    public function tempOptimum($week){
        $max=$this->tempOptimumMax[$week];
        $min=$this->tempOptimumMin[$week];
        $range["max"]=$max;
        $range["min"]=$min;
        return $range;
    }
    //Returns tolerable temperature range for a particular week
    public function tempTolerable($week){
        $max=$this->tempTolerableMax[$week];
        $min=$this->tempTolerableMin[$week];
        $range["max"]=$max;
        $range["min"]=$min;
        return $range;
    }

    //Returns optimal precipitation range for a particular week
    public function rainOptimum($week){
        $max=$this->rainOptimumMax[$week];
        $min=$this->rainOptimumMin[$week];
        $range["max"]=$max;
        $range["min"]=$min;
        return $range;
    }
    //Returns tolerable precipitation range for a particular week
    public function rainTolerable($week){
        $max=$this->rainTolerableMax[$week];
        $min=$this->rainTolerableMin[$week];
        $range["max"]=$max;
        $range["min"]=$min;
        return $range;
    }

    //triggered at the end of each week until harvest ready
    public function updateWeeks(){
        $this->weeksToHarvest--;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getWeeksToHarvest()
    {
        return $this->weeksToHarvest;
    }
    public function setWeeksToHarvest($weeksToHarvest)
    {
        $this->weeksToHarvest = $weeksToHarvest;
    }

    public function getRainOptimumMax()
    {
        return $this->rainOptimumMax;
    }
    public function setRainOptimumMax($rainOptimumMax)
    {
        $this->rainOptimumMax = $rainOptimumMax;
    }

    public function getRainOptimumMin()
    {
        return $this->rainOptimumMin;
    }
    public function setRainOptimumMin($rainOptimumMin)
    {
        $this->rainOptimumMin = $rainOptimumMin;
    }

    public function getRainTolerableMax()
    {
        return $this->rainTolerableMax;
    }
    public function setRainTolerableMax($rainTolerableMax)
    {
        $this->rainTolerableMax = $rainTolerableMax;
    }

    public function getRainTolerableMin()
    {
        return $this->rainTolerableMin;
    }
    public function setRainTolerableMin($rainTolerableMin)
    {
        $this->rainTolerableMin = $rainTolerableMin;
    }

    public function getTempOptimumMax()
    {
        return $this->tempOptimumMax;
    }
    public function setTempOptimumMax($tempOptimumMax)
    {
        $this->tempOptimumMax = $tempOptimumMax;
    }

    public function getTempOptimumMin()
    {
        return $this->tempOptimumMin;
    }
    public function setTempOptimumMin($tempOptimumMin)
    {
        $this->tempOptimumMin = $tempOptimumMin;
    }

    public function getTempTolerableMax()
    {
        return $this->tempTolerableMax;
    }
    public function setTempTolerableMax($tempTolerableMax)
    {
        $this->tempTolerableMax = $tempTolerableMax;
    }

    public function getTempTolerableMin()
    {
        return $this->tempTolerableMin;
    }
    public function setTempTolerableMin($tempTolerableMin)
    {
        $this->tempTolerableMin = $tempTolerableMin;
    }


}