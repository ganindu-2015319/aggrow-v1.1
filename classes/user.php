<?php
/**
 * Created by PhpStorm.
 * User: Stig
 * Date: 27/04/2019
 * Time: 10:59 AM
 */

class user
{
    var $userName;
    public $primaryCrop;
    public $secondaryCrop;
    public $recommendedPrimary=array();
    public $recommendedSecondary=array();

//Returns a list of all crops that are compatible with the forecast
//Accepts array of Crop objects and the climate forecast as parameters
    public function getPrimaryCrops(climateForecast $climateForecast, array $cropList){
        foreach ($cropList as $crop){
            $compatibility= $climateForecast->cropCompatibility($crop);
            if ($compatibility['level']>=1){
                $this->recommendedPrimary[]=array("crop" => $crop , "week"=>$compatibility['week']);
                echo '<br>';
            }
        }
        return $this->recommendedPrimary;
    }
    public function clearPrimaryRecommendations(){
        $this->recommendedPrimary=array();
    }

//Returns a list of all crops that are compatible with the selected primary crop
//Accepts array of cropNames and the climate forecast as parameters
    public function getSecondaryCrops(){
        $prim=$this->primaryCrop;
        $compatible=$prim->getCompatibleCrops();
        foreach ($compatible as $cropName){
                foreach ($this->recommendedPrimary as $entry){
                    if($cropName==$entry['crop']->getName()){
                        $this->recommendedSecondary[]=array("crop" => $entry['crop'] , "week"=>$entry['week']);
                    }
            }
        }
        return $this->recommendedSecondary;
    }
    public function clearSecondaryRecommendations(){
        $this->recommendedSecondary=array();
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
    }
    public function getUserName()
    {
        return $this->userName;
    }

    public function setPrimaryCrop($primaryCropName)
    {
        foreach ($this->recommendedPrimary as $crop){
            if ($crop['crop']->getName()==$primaryCropName){
                $this->primaryCrop = $crop['crop'];
                break;
            }


        }

    }
    public function getPrimaryCrop()
    {
        return $this->primaryCrop;
    }

    public function setRecommendedPrimary($recommendedPrimary)
    {
        $this->recommendedPrimary = $recommendedPrimary;
    }
    public function getRecommendedPrimary()
    {
        return $this->recommendedPrimary;
    }

    public function setRecommendedSecondary($recommendedSecondary)
    {
        $this->recommendedSecondary = $recommendedSecondary;
    }
    public function getRecommendedSecondary()
    {
        return $this->recommendedSecondary;
    }

    public function setSecondaryCrop($secondaryCropName)
    {
        foreach ($this->recommendedSecondary as $crop){
//            var_dump($this->recommendedPrimary);
            if ($crop['crop']->getName()==$secondaryCropName){
                $this->secondaryCrop = $crop['crop'];
                break;
            }


        }
    }
    public function getSecondaryCrop()
    {
        return $this->secondaryCrop;
    }

}